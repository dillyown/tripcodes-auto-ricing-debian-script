#!/bin/bash

#Sanity check
if [ -z "$1" ]
then
	echo "The /dev/???/I'm tired"
	exit
fi

if [ -z "$2" ]
then
	echo "The username is empty"
	exit
fi

if [ -z "$3" ]
then
	echo "The password is empty"
	exit
fi

if [ -z "$4" ]
then
	echo "Need the size of the partition"
	exit
fi

#Make sure drive is unmounted
sudo umount -l /mnt

#format the drive
(
echo o # Create a new empty DOS partition table
echo n # Add a new partition for root
echo p 
echo  
echo   
echo $4  #This value determines swap size
echo a
echo
echo n
echo p
echo
echo 
echo 
echo w # Write changes
) | sudo fdisk $1 || exit 1

sudo mkfs.ext4 $11 #Format root
sudo mkswap $12 #Format swap
sudo mount $11 /mnt #Mount partition

#Run debootstrap exits if this fails choose archetecture exp: i386 amd64
yes | sudo debootstrap --arch=amd64 stretch /mnt http://ftp.us.debian.org/debian 

#Copy files for fstab and sources
sudo cp fstab /mnt/etc/fstab
sudo cp sources.list /mnt/etc/apt/sources.list
sudo cp default-release /mnt/etc/apt/apt.conf.d

#Mount more directories
sudo mount -t sysfs sysfs /mnt/sys
sudo mount -t proc proc /mnt/proc
sudo mount -o bind /dev /mnt/dev

#This positions the chroot script
sudo cp ./chroot.sh /mnt/chroot.sh 

#This positions the i3-gaps-deb install script.
sudo cp i3-gaps-deb /mnt/i3-gaps-deb

#This runs the chroot script
#Feeds in directory username and password
sudo chroot /mnt /chroot.sh $1 $2 $3

#Clean up and copy remaining files
sudo rm /mnt/chroot.sh
sudo mkdir /mnt/home/$2/backgrounds/
sudo mkdir /mnt/home/$2/screenshots
sudo cp backgrounds/* /mnt/home/$2/backgrounds/
sudo cp bash_profile /mnt/home/$2/.bash_profile

#Copy files in files directory
sudo cp --verbose -R files/* /mnt/home/$2/

#More copying
sudo mkdir /mnt/home/$2/.urxvt/
sudo mkdir /mnt/home/$2/.urxvt/ext/
sudo cp font-size /mnt/home/$2/.urxvt/ext/
sudo mkdir /mnt/home/$2/.config/
sudo mkdir /mnt/home/$2/.config/i3/
sudo cp i3_config /mnt/home/$2/.config/i3/config
sudo cp Xresources /mnt/home/$2/.Xdefaults
sudo mkdir /mnt/home/$2/.config/i3status
sudo cp i3status /mnt/home/$2/.config/i3status/config
sudo cp rc.conf /mnt/home/$2/rc.conf
#This line chroots to fix permissions
sudo chroot /mnt chown $2 -R /home/$2



export TERM=xterm-256color 

#This creates a new user
(
echo $3
echo $3
echo
echo
echo
echo
echo
echo y
) | adduser $2

#This section sets the time and locales
echo "Setting local time to chicago\n"
ln -sf /usr/share/zoneinfo/America/Chicago /etc/localtime
yes | dpkg-reconfigure locales

#Sets the hosts
printf '127.0.0.1\t debian'>>/etc/hosts

#Sets the root password
(
echo $3
echo $3
) | passwd

#This installs the required packages
yes | apt-get update && apt-get upgrade || exit 1
yes | apt-get install sudo firmware-iwlwifi linux-image-amd64 initramfs-tools vim-gtk chromium ffmpeg network-manager-gnome htop xorg i3 feh scrot neofetch git kazam rxvt-unicode mpv pavucontrol pulseaudio || exit 1



#This installs grub
echo y | apt-get install grub2
yes | apt-get install -t testing gcc-7 ranger

#This sets the user to group sudo
echo "Set user"
adduser $2 sudo
(
echo
echo
echo
echo
echo y
echo
echo y
echo y
echo
echo
) | ./i3-gaps-deb

#Updates grub
update-grub
